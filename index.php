<?php

require_once('animal1.php');
require_once('animal2.php');

$sheep = new animal("shaun");

echo "<br>";
echo "Name : " . $sheep->name . "<br>";
echo "Legs:  " . $sheep->legs . "<br>";
echo "clodblooded : " . $sheep->cold_blooded . "<br>";

$kodok = new Frog("Buduk");

echo "<br>";
echo "Name : " . $kodok->name . "<br>";
echo "Legs  : " . $kodok->legs . "<br>";
echo "clodblooded : " . $kodok->clod_blooded . "<br>";
echo "Jump : " . $kodok->jump . "<br>";

$sungokong = new Ape("kera sakti");

echo "<br>";
echo "Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "clodblooded : " . $sungokong->clod_blooded . "<br>";
echo "Yell : " . $sungokong->yel . "<br>";
